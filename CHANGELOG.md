# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.2"></a>
## [0.1.2](https://github.com/pixelass/ponywheel/compare/v0.1.1...v0.1.2) (2016-09-07)


### Bug Fixes

* **listeners:** remove listener ([1cf47b4](https://github.com/pixelass/ponywheel/commit/1cf47b4))



<a name="0.1.1"></a>
## [0.1.1](https://github.com/pixelass/ponywheel/compare/v0.1.0...v0.1.1) (2016-09-05)



<a name="0.1.0"></a>
# 0.1.0 (2016-09-05)


### Features

* **main:** main featureset ([4309a67](https://github.com/pixelass/ponywheel/commit/4309a67))



<a name="0.1.1"></a>
## [0.1.1](https://github.com/pixelass/onwheel-fix/compare/v0.1.0...v0.1.1) (2016-09-02)



<a name="0.1.0"></a>
# 0.1.0 (2016-09-02)


### Features

* **initial:** initial featureset ([fc9a2a9](https://github.com/pixelass/onwheel-fix/commit/fc9a2a9))
