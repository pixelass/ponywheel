
/**
 * debounce time
 * @type {Number}
 */
const DELAY = 50
/**
 * flag to determine touch devices
 * @type {Boolean}
 */
const touchNeedsHelp = ('ontouchstart' in document.documentElement)

/**
 * adds mousewheel for touch devices
 *
 */
class PonyWheel {
  /**
   * @constructor
   * @param  {Object} props Options
   * @param  {Object} props.rootNode usually document.body. Can be any decendant of document.body
   * @param  {Object} props.eventTarget usually window. Can be same as `rootNode`
   * @return {this}   returns an instance of PonyWheel
   */
  constructor(props) {
    this.rootNode = props.rootNode
    this.eventTarget = props.eventTarget

    /**
     * touch data
     * @type {Object}
     */
    this.touch = {
      x: 0,
      y: 0,
      allow: false
    }
    /**
     * mouse data
     * @type {Object}
     */
    this.mouse = {
      x: 0,
      y: 0
    }

    /**
     * timer to debounce tracking
     */
    this.timer = setTimeout(() => {}, DELAY)

    // bindings
    this.extendMouse = this.extendMouse.bind(this)
    this.startTouch = this.startTouch.bind(this)
    this.doTouch = this.doTouch.bind(this)
    this.endTouch = this.endTouch.bind(this)
    this.onWheel = this.onWheel.bind(this)
  }

  /**
   * initialize the ponyfill.
   */
  init () {
    if (touchNeedsHelp) {
      this.eventTarget.addEventListener('touchstart', this.startTouch, false)
      this.eventTarget.addEventListener('touchmove', this.doTouch, false)
      this.eventTarget.addEventListener('touchend', this.endTouch, false)
    } else {
      this.eventTarget.addEventListener('wheel', this.onWheel, false)
    }
  }
  /**
   * remove the ponyfill.
   */
  destroy() {
   if (touchNeedsHelp) {
      this.eventTarget.removeEventListener('touchstart', this.startTouch)
      this.eventTarget.removeEventListener('touchmove', this.doTouch)
      this.eventTarget.removeEventListener('touchend', this.endTouch)
      this.eventTarget.removeEventListener('scroll', this.extendMouse)
    } else {
      this.eventTarget.removeEventListener('wheel', this.onWheel)
    }
  }

  /**
   * Wheels don't move so let's strap a pony to our wheels
   * and take a ride to wonderland
   * @param  {Object} detail custom event data
   */
  onWheel(detail) {
    const event = new CustomEvent('ponywheel', {detail})
    this.eventTarget.dispatchEvent(event)
  }

  /**
   * handles touch interaction. only executed when touch is allowed
   * @param  {Object} e the touchstart event
   * @return {undefined}   return `undefined` if touch is not allowed
   */
  doTouch (e) {
    // stop here if touch is not allowed
    if (!this.touch.allow) {
      return
    }

    // mouse data
    const scrollTop = Math.max(0, this.rootNode.scrollTop)
    this.mouse.x = this.rootNode.scrollLeft
    this.mouse.y = scrollTop
    // touch data
    // calculate the delta and reassign position
    const deltaX = ~~(this.touch.x -  e.touches[0].clientX)
    const deltaY = ~~(this.touch.y -  e.touches[0].clientY)
    this.touch.x = e.touches[0].clientX
    this.touch.y = e.touches[0].clientY

    // assign new props to event
    Object.assign(e, {
      deltaX,
      deltaY
    })
    this.onWheel(e, true)
  }

  /**
   * extend the mouse by deltaX and deltaY
   * @param  {Object} e event that triggered this method
   */
  extendMouse (e) {
    clearTimeout(this.timer)
    const mouse = {
      x: this.rootNode.scrollLeft,
      y: Math.max(0, this.rootNode.scrollTop)
    }
    const deltaX = ~~(this.mouse.x - this.rootNode.scrollLeft) * -1
    const deltaY = ~~(this.mouse.y - scrollTop) * -1

    this.mouse = {
      x: this.rootNode.scrollLeft,
      y: Math.max(0, this.rootNode.scrollTop)
    }
    Object.assign(e, {
      deltaX,
      deltaY
    })

    this.onWheel(e)
    this.timer = setTimeout(() => {
      this.eventTarget.removeEventListener('scroll', this.extendMouse)
    }, DELAY)
  }

  /**
   * touching has ended. reset flags and assign scroll listener.
   * The scroll listener is used to add momentum.
   */
  endTouch () {
    this.touch.allow = false
    const scrollTop = Math.max(0, this.rootNode.scrollTop)
    this.mouse = {
      x: this.rootNode.scrollLeft,
      y: Math.max(0, this.rootNode.scrollTop)
    }
    this.eventTarget.addEventListener('scroll', this.extendMouse, false)
  }

  /**
   * when the user touches the screen, set some defaults.
   * The defaults will be read and modified on `doTouch`
   * @param  {Object} e the touchstart event
   */
  startTouch (e) {
    const scrollTop = Math.max(0, this.rootNode.scrollTop)
    this.touch.x = e.touches[0].clientX
    this.touch.y = e.touches[0].clientY
    this.touch.allow = true
    this.mouse.x = this.rootNode.scrollLeft
    this.mouse.y = scrollTop
    this.eventTarget.removeEventListener('scroll', this.extendMouse)
  }
}

export default PonyWheel
