# ponywheel
Mousewheel for touch devices

```shell
npm install ponywheel
```
[Documentation](https://pixelass.github.io/ponywheel/)

[![npm](https://img.shields.io/npm/v/ponywheel.svg)](https://www.npmjs.com/package/ponywheel)
[![GitHub license](https://img.shields.io/github/license/pixelass/ponywheel.svg)](https://github.com/pixelass/ponywheel/blob/master/LICENSE)
[![Travis](https://img.shields.io/travis/pixelass/ponywheel.svg)](https://travis-ci.org/pixelass/ponywheel)
[![David](https://img.shields.io/david/pixelass/ponywheel.svg)](https://david-dm.org/pixelass/ponywheel)
[![David](https://img.shields.io/david/dev/pixelass/ponywheel.svg)](https://david-dm.org/pixelass/ponywheel#info=devDependencies&view=table)
[![GitHub issues](https://img.shields.io/github/issues/pixelass/ponywheel.svg)](https://github.com/pixelass/ponywheel/issues)
[![GitHub forks](https://img.shields.io/github/forks/pixelass/ponywheel.svg)](https://github.com/pixelass/ponywheel/network)
[![GitHub stars](https://img.shields.io/github/stars/pixelass/ponywheel.svg)](https://github.com/pixelass/ponywheel/stargazers)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)


## Usage

```js
const ponyWheel = new PonyWheel(
  {
    rootNode: document.body,
    eventTarget: window
  });

ponyWheel.init()

window.addEventListener('ponywheel',(e)=> {
  console.log(e.detail) // => mousewheel.e
})

// ponyWheel.destroy()

```
