(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _src = require('./src');

var _src2 = _interopRequireDefault(_src);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

window.PonyWheel = _src2.default; // global window

},{"./src":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ponywheel = require('./ponywheel');

var _ponywheel2 = _interopRequireDefault(_ponywheel);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = _ponywheel2.default;

},{"./ponywheel":3}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

/**
 * debounce time
 * @type {Number}
 */
var DELAY = 50;
/**
 * flag to determine touch devices
 * @type {Boolean}
 */
var touchNeedsHelp = 'ontouchstart' in document.documentElement;

/**
 * adds mousewheel for touch devices
 *
 */

var PonyWheel = function () {
  /**
   * @constructor
   * @param  {Object} props Options
   * @param  {Object} props.rootNode usually document.body. Can be any decendant of document.body
   * @param  {Object} props.eventTarget usually window. Can be same as `rootNode`
   * @return {this}   returns an instance of PonyWheel
   */
  function PonyWheel(props) {
    _classCallCheck(this, PonyWheel);

    this.rootNode = props.rootNode;
    this.eventTarget = props.eventTarget;

    /**
     * touch data
     * @type {Object}
     */
    this.touch = {
      x: 0,
      y: 0,
      allow: false
    };
    /**
     * mouse data
     * @type {Object}
     */
    this.mouse = {
      x: 0,
      y: 0
    };

    /**
     * timer to debounce tracking
     */
    this.timer = setTimeout(function () {}, DELAY);

    // bindings
    this.extendMouse = this.extendMouse.bind(this);
    this.startTouch = this.startTouch.bind(this);
    this.doTouch = this.doTouch.bind(this);
    this.endTouch = this.endTouch.bind(this);
    this.onWheel = this.onWheel.bind(this);
  }

  /**
   * initialize the ponyfill.
   */

  _createClass(PonyWheel, [{
    key: 'init',
    value: function init() {
      if (touchNeedsHelp) {
        this.eventTarget.addEventListener('touchstart', this.startTouch, false);
        this.eventTarget.addEventListener('touchmove', this.doTouch, false);
        this.eventTarget.addEventListener('touchend', this.endTouch, false);
      } else {
        this.eventTarget.addEventListener('wheel', this.onWheel, false);
      }
    }
    /**
     * remove the ponyfill.
     */

  }, {
    key: 'destroy',
    value: function destroy() {
      if (touchNeedsHelp) {
        this.eventTarget.removeEventListener('touchstart', this.startTouch);
        this.eventTarget.removeEventListener('touchmove', this.doTouch);
        this.eventTarget.removeEventListener('touchend', this.endTouch);
        this.eventTarget.removeEventListener('scroll', this.extendMouse);
      } else {
        this.eventTarget.removeEventListener('wheel', this.onWheel);
      }
    }

    /**
     * Wheels don't move so let's strap a pony to our wheels
     * and take a ride to wonderland
     * @param  {Object} detail custom event data
     */

  }, {
    key: 'onWheel',
    value: function onWheel(detail) {
      var event = new CustomEvent('ponywheel', { detail: detail });
      this.eventTarget.dispatchEvent(event);
    }

    /**
     * handles touch interaction. only executed when touch is allowed
     * @param  {Object} e the touchstart event
     * @return {undefined}   return `undefined` if touch is not allowed
     */

  }, {
    key: 'doTouch',
    value: function doTouch(e) {
      // stop here if touch is not allowed
      if (!this.touch.allow) {
        return;
      }

      // mouse data
      var scrollTop = Math.max(0, this.rootNode.scrollTop);
      this.mouse.x = this.rootNode.scrollLeft;
      this.mouse.y = scrollTop;
      // touch data
      // calculate the delta and reassign position
      var deltaX = ~~(this.touch.x - e.touches[0].clientX);
      var deltaY = ~~(this.touch.y - e.touches[0].clientY);
      this.touch.x = e.touches[0].clientX;
      this.touch.y = e.touches[0].clientY;

      // assign new props to event
      Object.assign(e, {
        deltaX: deltaX,
        deltaY: deltaY
      });
      this.onWheel(e, true);
    }

    /**
     * extend the mouse by deltaX and deltaY
     * @param  {Object} e event that triggered this method
     */

  }, {
    key: 'extendMouse',
    value: function extendMouse(e) {
      var _this = this;

      clearTimeout(this.timer);
      var mouse = {
        x: this.rootNode.scrollLeft,
        y: Math.max(0, this.rootNode.scrollTop)
      };
      var deltaX = ~~(this.mouse.x - this.rootNode.scrollLeft) * -1;
      var deltaY = ~~(this.mouse.y - scrollTop) * -1;

      this.mouse = {
        x: this.rootNode.scrollLeft,
        y: Math.max(0, this.rootNode.scrollTop)
      };
      Object.assign(e, {
        deltaX: deltaX,
        deltaY: deltaY
      });

      this.onWheel(e);
      this.timer = setTimeout(function () {
        _this.eventTarget.removeEventListener('scroll', _this.extendMouse);
      }, DELAY);
    }

    /**
     * touching has ended. reset flags and assign scroll listener.
     * The scroll listener is used to add momentum.
     */

  }, {
    key: 'endTouch',
    value: function endTouch() {
      this.touch.allow = false;
      var scrollTop = Math.max(0, this.rootNode.scrollTop);
      this.mouse = {
        x: this.rootNode.scrollLeft,
        y: Math.max(0, this.rootNode.scrollTop)
      };
      this.eventTarget.addEventListener('scroll', this.extendMouse, false);
    }

    /**
     * when the user touches the screen, set some defaults.
     * The defaults will be read and modified on `doTouch`
     * @param  {Object} e the touchstart event
     */

  }, {
    key: 'startTouch',
    value: function startTouch(e) {
      var scrollTop = Math.max(0, this.rootNode.scrollTop);
      this.touch.x = e.touches[0].clientX;
      this.touch.y = e.touches[0].clientY;
      this.touch.allow = true;
      this.mouse.x = this.rootNode.scrollLeft;
      this.mouse.y = scrollTop;
      this.eventTarget.removeEventListener('scroll', this.extendMouse);
    }
  }]);

  return PonyWheel;
}();

exports.default = PonyWheel;

},{}]},{},[1]);
